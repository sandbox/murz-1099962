The DomainField module creates the new CCK field type that can store different text 
for each domain, created via Domain Access module.

DomainField was written by Alexey Murz Korepov.

Dependencies
------------
 * Content
 * Domain Access

Install
-------

1) Copy the domainfield folder to the modules folder in your installation.

2) Enable the module using Administer -> Site building -> Modules
   (/admin/build/modules).

3) Create a new domain field in through CCK's interface. Visit Administer ->
   Content management -> Content types (admin/content/types), then click
   Manage fields on the type you want to add an domain field. Select field 
   type as the widget type to create a new field.

Known Issues
------------

1) For normal work at now you should disable site cache or create the separate
caches for each domain via domain_prefix module. I try to solve this problem 
in future version of module.

2) This module works out of the box, but at now shows SQL errors when creating
fields. This is because CCK module have unresolved bug #1091716 . You can patch
the CCK module or ignore those errors, without patching module works normally too.